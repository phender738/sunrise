const https = require('https');
const querystring = require('querystring');
const Joi = require('joi');
const debugError = require("debug")("sunrise:sunriseAPI_error");
const debugInfo = require("debug")("sunrise:sunriseAPI_info");
const EventEmitter = require('events');
const R = require('ramda');


class Sunrise extends EventEmitter {
    constructor(uri) {
        super();
        this.sunriseEndpoint = uri;
    }

    async doBatchCall(coords, simultaneousCalls, intervalbetweenCalls) {

        let returnArr = [];
        for (let i = 0; i < coords.length; i += simultaneousCalls) {
            //make a deep copy of X objects
            let copyCoord = R.clone(coords.slice(i, i + simultaneousCalls));
            returnArr.push.apply(returnArr, await this.doIteration(copyCoord));
            await this.asyncSleep(intervalbetweenCalls);

        }
        return returnArr;
    }

    doIteration(coords) {
        const promises = coords.map(c => this.doHTTPSRequest(c));
        return Promise.all(promises);
    }

    /**
     * Returns calls to endpoint.
     * @constructor
     * @param {int} simultaneousCalls - How many concurrent calls to make.
     * @param {int} intervalbetweenCalls - The interval between each batch of concurrent calls.
     */
    getSunBatch(coords, simultaneousCalls, intervalbetweenCalls) {
        this.emit('requestStarted');
        return new Promise((resolve, reject) => {
            this.doBatchCall(coords, simultaneousCalls, intervalbetweenCalls)
                .then((returnArr) => {
                    resolve(returnArr);
                })
                .catch(err => {
                    reject(err);
                });
        });

    }


    asyncSleep(milli) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
            }, milli);
        });
    }


    getIndexEarliestSunrise(coords) {
        let earliestIndex;
        const earliestSunrise = coords.reduce(function (accumulator, currentValue, index) {
            if (currentValue.results.sunriseTimestamp < 10000) return accumulator;//to address a bug in the API, which sometimes returns an invalid time for sunrise
            if (accumulator < currentValue.results.sunriseTimestamp) return accumulator;
            earliestIndex = index;
            return currentValue.results.sunriseTimestamp;
        }, coords[0].results.sunriseTimestamp);
        debugInfo('EarlistIndex = ', earliestIndex);
        return earliestIndex;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //doAsyncCall(){}
    doHTTPSRequest(coordObj) {
        return new Promise((resolve, reject) => {
            //validate the input
            if (!this.validateCoord(coordObj)) reject(new Error('invalid request'));

            const request = this.sunriseEndpoint + querystring.stringify(coordObj) + '&formatted=0';
            debugInfo('Request string: ', request);

            https.get(request, (resp) => {
                var data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    try {
                        const retObj = JSON.parse(data);
                        //convert the sunrise time into a timestamp and add it to the returning object 
                        retObj.results.sunriseTimestamp = new Date(retObj.results.sunrise).getTime();
                        retObj.results.lng = coordObj.lng;
                        retObj.results.lat = coordObj.lat;
                        resolve(retObj);
                        this.emit('eachSingleRequest', retObj);
                    } catch (e) {
                        reject(e);
                    }

                    return;
                });

                resp.on("error", (err) => {
                    reject(err);
                    return;
                });

            }).on('error', (err) => {
                debugError("Error: " + err);
                reject(err);
                return;
            });


        });
    }

    validateCoord(coord) {
        const schema = {
            long: Joi.number().required(),
            lat: Joi.number().required()
        };
        return Joi.validate(coord, schema);
    }


}

module.exports = Sunrise;

