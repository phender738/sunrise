const express = require("express");
const app = express();
//const config = require("config");
const Sunrise = require("./sunrise");


const debugError = require("debug")("sunrise:index_error");
const debugInfo = require("debug")("sunrise:index_info");



//could be shifted int a config module
const simultaneousCalls = 5;
const intervalbetweenCalls = 5000; //Milli seconds
const coordinatesCount = 100;
const endpointUri = 'https://api.sunrise-sunset.org/json?';

const generateRandomCoord = function () {
    return {
        lat: Math.random() * (180 - -180) + -180,
        lng: Math.random() * (90 - -90) + -90
    };
}
//generate 100 random coordinates
const coords = Array.from({ length: coordinatesCount }, generateRandomCoord);

const sunInfo = new Sunrise(endpointUri);


//events
sunInfo
    .on('requestStarted', () => {
        console.log('Batch Request Started');
    })
    .on('error', (err) => {
        debugError(err.message);
    })
    .on('eachSingleRequest', (obj) => {
        console.log('Requesting longitude: ' + obj.results.lng + 'Lattitude: ' + obj.results.lat);
        debugInfo('Sunrise Timestamp:', obj.results.sunriseTimestamp);
    });


async function getAllSun(coords, simultaneousCalls, intervalbetweenCalls) {
    try {
        const aCoords = await sunInfo.getSunBatch(coords, simultaneousCalls, intervalbetweenCalls);
        const earliestSunsetIndex = sunInfo.getIndexEarliestSunrise(aCoords);
        console.log('Earliest Sunrise:', new Date(aCoords[earliestSunsetIndex].results.sunriseTimestamp));

        //show the earliest sunrise
        console.log(aCoords[earliestSunsetIndex]);


    } catch (err) {
        debugError('Error ', err.message);
    }



}


getAllSun(coords, simultaneousCalls, intervalbetweenCalls);